FROM mcr.microsoft.com/dotnet/core/sdk:3.1
RUN apt update
RUN apt install default-jdk -y
RUN dotnet tool install --global dotnet-sonarscanner
ENV PATH "$PATH:/root/.dotnet/tools"